import numpy as np 
from sklearn.kernel_ridge import KernelRidge
from PCAfold import KReg
from sklearn.datasets import make_friedman2
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import DotProduct, WhiteKernel, RBF
import math 
import faiss 

def rel_err(y_true, y_pred):
    """
    return for ||y_true - y_pred|| / ||y_true|| + epsilon
    """
    return np.linalg.norm(y_pred - y_true) / max(np.linalg.norm(y_true), 1e-4)

def knn_gpr_average(X, Y, k, Query_Point, Query_Point_y, return_error = True):
    """
    default return relative error for each term 
    or return the prediction of X.
    
    X: the training points of X added to create dictionary for knn search
    Y: corresponding training points of Y
    Query_Point: the test point to be evaluated 
    Query_Point_y: true responding value at each test point 
    k: number of neighbours in knn search
    """
    
    # Checks on the sizes of input variables:
    (n_observations_X, n_variables_X) = np.shape(X)
    (n_observations_Y, n_variables_Y) = np.shape(Y)
    (n_observations_Query_Point, n_variables_Query_Point) = np.shape(Query_Point)
    (n_observations_Query_Point_y, n_variables_Query_Point_y) = np.shape(Query_Point_y)
    
    if n_observations_X != n_observations_Y:
        raise ValueError("Number of training observations not consistent.")
        
    if n_observations_Query_Point != n_observations_Query_Point_y:
        raise ValueError("Number of query observations not consistent.")
    
    if n_variables_X != n_variables_Query_Point:
        raise ValueError("Number of independent variables not consistent between training and query points.")
        
    if  n_variables_Y != n_variables_Query_Point_y:
        raise ValueError("Number of dependent variables not consistent between training and query points.")
    
    n,d = Query_Point.shape
    n,d1 = Query_Point_y.shape
    index = faiss.IndexFlatL2(d)
    index.add(X)
    if return_error:
        var_y_1 = np.zeros(n)
    else:
        var_y_1 = np.zeros((n,d1))
    def gaussian_kernel(z):
        return (np.exp(-0.5 * z**2))
    for j in range(n):
        D,I = index.search((Query_Point[j,:]).reshape((1,d)),k)
        b = np.max([np.linalg.norm(Query_Point[j,:] - x) for x in X[I[0],:]]) 
        l, D_1, I_1 = index.range_search((Query_Point[j,:]).reshape((1,d)), 2 * b ** 2)
        mask = np.array([x not in [j] for x in I_1])
        I_2 = I_1[mask,...]
        new_x = X[I_2,:] 
        knn_y_i = Y[I_2,:]
        gpr = GaussianProcessRegressor(
          random_state=0,copy_X_train=False).fit(new_x, knn_y_i)
        pred = gpr.predict(Query_Point[j,:].reshape([1,3]))
        if return_error:
            var_y_1[j] = rel_err(pred[0],Query_Point_y[j,:])
        else:
            var_y_1[j,:] = pred[0]
    return var_y_1 


def gaussian_kernel(z):
    return (np.exp(-0.5 * z**2))
def singular_kernel(z,d):
    return np.linalg.norm(z)**(-d) if np.linalg.norm(z) > 1 else 0
def singular_kernel_plus(z,d):
    return np.linalg.norm(z)**(-d) * (max(1 - np.linalg.norm(z), 0))**2
def singular_kernel_minus(z,d):
    return np.linalg.norm(z)**(-d) if np.linalg.norm(z) <= 1 else 0
def singular_kernel_cosine(z,d):
    return np.linalg.norm(z)**(-d) * np.cos(math.pi * np.linalg.norm(z) / 2)**2 if np.linalg.norm(z) <= 1 else 0

def singular_kernel_average(X,Y,k,kernel,Query_Point, Query_Point_y, return_error = True):
    """
    X: the training points of X added to create dictionary for knn search
    Y: corresponding traininng points of Y
    Query_Point: the test point to be evaluated 
    Query_Point_y: true responding value at each test point 
    k: number of neighbours in knn search
    kernel: the kernel used in singular_kernel_estimator
    """
    
    # Checks on the sizes of input variables:
    (n_observations_X, n_variables_X) = np.shape(X)
    (n_observations_Y, n_variables_Y) = np.shape(Y)
    (n_observations_Query_Point, n_variables_Query_Point) = np.shape(Query_Point)
    (n_observations_Query_Point_y, n_variables_Query_Point_y) = np.shape(Query_Point_y)
    
    if n_observations_X != n_observations_Y:
        raise ValueError("Number of training observations not consistent.")
        
    if n_observations_Query_Point != n_observations_Query_Point_y:
        raise ValueError("Number of query observations not consistent.")
    
    if n_variables_X != n_variables_Query_Point:
        raise ValueError("Number of independent variables not consistent between training and query points.")
        
    if  n_variables_Y != n_variables_Query_Point_y:
        raise ValueError("Number of dependent variables not consistent between training and query points.")
        
    n,d = Query_Point.shape
    n,d1 = Query_Point_y.shape
    index = faiss.IndexFlatL2(d)
    index.add(X)
    if return_error:
        var_y_1 = np.zeros(n)
    else:
        var_y_1 = np.zeros((n,d1))
    def nw_kernel(train_x, train_y, query_x, kernel, h):
        kernel_x = np.array([kernel((query_x - x)/h, d) for x in train_x])
        sum_kernel_x = np.sum(kernel_x)
        if sum_kernel_x == 0:
           return 0
        sum_kernel_y = np.sum((train_y.T * kernel_x).T,axis = 0)
        return sum_kernel_y / sum_kernel_x
    for j in range(n):
        D,I = index.search((Query_Point[j,:]).reshape((1,d)),k)
        b = np.max([np.linalg.norm(Query_Point[j,:] - x) for x in X[I[0],:]]) 
        b1 = float(b)
        l, D_1, I_1 = index.range_search((Query_Point[j,:]).reshape((1,d)), 2 * b ** 2)
        mask = np.array([x not in [j] for x in I_1])
        I_2 = I_1[mask,...]
        new_x = X[I_2,:] 
        knn_y_i = Y[I_2,:]
        pred = nw_kernel(new_x, knn_y_i, Query_Point[j,:], kernel, b1)
        if return_error:
            var_y_1[j] = rel_err(pred,Query_Point_y[j,:])
        else:
            var_y_1[j,:] = pred
    return var_y_1

def knn_kreg_average(X,Y,k,Query_Point,Query_Point_y, return_error = True):
    """
    X: the training points of X added to create dictionary for knn search
    Y: corresponding traininng points of Y
    Query_Point: the test point to be evaluated 
    Query_Point_y: true responding value at each test point 
    k: number of neighbours in knn search
    """
    
    # Checks on the sizes of input variables:
    (n_observations_X, n_variables_X) = np.shape(X)
    (n_observations_Y, n_variables_Y) = np.shape(Y)
    (n_observations_Query_Point, n_variables_Query_Point) = np.shape(Query_Point)
    (n_observations_Query_Point_y, n_variables_Query_Point_y) = np.shape(Query_Point_y)
    
    if n_observations_X != n_observations_Y:
        raise ValueError("Number of training observations not consistent.")
        
    if n_observations_Query_Point != n_observations_Query_Point_y:
        raise ValueError("Number of query observations not consistent.")
    
    if n_variables_X != n_variables_Query_Point:
        raise ValueError("Number of independent variables not consistent between training and query points.")
        
    if  n_variables_Y != n_variables_Query_Point_y:
        raise ValueError("Number of dependent variables not consistent between training and query points.")
        
    n,d = Query_Point.shape
    n,d1 = Query_Point_y.shape
    index = faiss.IndexFlatL2(d)
    index.add(X)
    if return_error:
        var_y_1 = np.zeros(n)
    else:
        var_y_1 = np.zeros((n,d1))
    def gaussian_kernel(z):
        return (np.exp(-0.5 * z**2))
    for j in range(n):
        D,I = index.search((Query_Point[j,:]).reshape((1,d)),k)
        b = np.max([np.linalg.norm(Query_Point[j,:] - x) for x in X[I[0],:]]) 
        l, D_1, I_1 = index.range_search((Query_Point[j,:]).reshape((1,d)), 2 * b ** 2)
        mask = np.array([x not in [j] for x in I_1])
        I_2 = I_1[mask,...]
        new_x = X[I_2,:] 
        knn_y_i = Y[I_2,:]
        b1 = float(b)
        def single_var_kernel_regression(variable):
            return KReg(new_x, variable[::,None]).predict(X[j,:].reshape([1,3]), bandwidth=b1)
        regressedSourceTerms = [single_var_kernel_regression(knn_y_i[:,k]) for k in range(d1)] 
        pred = np.array([regressedSourceTerms[0][0],regressedSourceTerms[1][0],regressedSourceTerms[2][0]])
        if return_error:
            var_y_1[j] = rel_err(pred,Query_Point_y[j,:])
        else:
            var_y_1[j,:] = pred
    return var_y_1 

def calculate_r2(y_true, y_pred):
    """
    calculate r-square 
    """
    sse = np.sum((y_pred - y_true)**2)
    sst = np.sum((y_true - np.mean(y_true))**2)
    return 1-sse/sst, np.sqrt(sse/len(y_pred))

def relative_error(y_true, y_pred, tolerance):
    """
    outdated function 
    """
    r_1 = np.sqrt(np.mean(((y_true - y_pred)/ (y_true + tolerance))**2))
    result = {}
    result["relative_error_true"] = r_1
    return result 

def norm_ratio(y_true, y_pred, epsilon):
    """
    return:  ||Y_true - Y_pred|| / ||Y_true|| + epsilon AND mean of that.
    """
    n = len(y_true)
    dist = np.zeros(n).reshape(n,1) # reshape for drawing in the plot_error funciton
    for i in range(n):
        dist[i,:] = np.linalg.norm(y_true[i,:] -y_pred[i,:]) / (np.linalg.norm(y_true[i,:]) + epsilon)
    return dist, np.mean(dist)


def cos_distance(y_true, y_pred):
    """
    return 1 - cos_similarity(y_true,y_pred)
    output size is same as y_true
    """
    n = len(y_true)
    dist = np.zeros(n).reshape(n,1)
    for i in range(n):
        dist[i,:] = 1 - np.dot(y_true[i,:]/np.linalg.norm(y_true[i,:]),y_pred[i,:]/np.linalg.norm(y_pred[i,:]))
    return dist

def same_direction(y_true, y_pred):
    """
    return count(I(<y_true, y_pred>) >= 0) / n
    output size is a single number 
    """
    n = len(y_true)
    count = 0 
    for i in range(n):
        count += np.dot(y_true[i,:],y_pred[i,:]) >= 0 
    return count / n 

def samd_d(y_true, y_pred):
    n = len(y_true)
    dist = np.zeros(n).reshape(n,1)
    for i in range(n):
        dist[i,:] = np.dot(y_true[i,:],y_pred[i,:]) >= 0 
    return dist

def run_kernel_regression(train_x, train_y, test_x, test_y, b, alpha, nw):
    """
    b: bandwidth parameter used in kernel ridge regression
    alpha: penalty parameter for kernel ridge regression 
    nw: if nw estimator is true, use kernel regression, else use kernel-ridge regression
    """
    n_dim = train_y.shape[1]
    rmse = []
    l_inf = []
    r_2 = []
    rel_1 = []
    if nw == True:
      def single_var_kernel_regression(variable):
          return KReg(train_x, variable[::,None]).predict(test_x, bandwidth=b)
      regressedSourceTerms = [single_var_kernel_regression(train_y[:,i]) for i in range(n_dim)] 
      pred_y = np.zeros((test_y.shape[0],test_y.shape[1]))
      for i in range(n_dim):
          pred_y[:,i] = [x for x in regressedSourceTerms[i]]
      pred_y = np.nan_to_num(pred_y)
    else:
      pred_y = np.zeros((test_y.shape[0],test_y.shape[1]))    

    for i in range(n_dim):
      if nw == False:
        clf = KernelRidge(alpha=alpha, kernel = "rbf", gamma = b).fit(train_x, train_y[:,i])
        pred_y[:,i] = clf.predict(test_x)
        pred_y = np.nan_to_num(pred_y)
        r_square, rmsr = calculate_r2(pred_y[:,i], test_y[:,i])
        r_2.append(r_square)
        rmse.append(rmsr)
      else:
        r_square, rmsr = calculate_r2(pred_y[:,i], test_y[:,i])
        r_2.append(r_square)
        rmse.append(rmsr)

      inf = np.max(np.abs(pred_y[:,i] - test_y[:,i]))
      l_inf.append(inf)
      res = relative_error(test_y[:,i], pred_y[:,i], 1e-14)
      rel_1.append(res["relative_error_true"])
    s_d = same_direction(pred_y, test_y)
    dist = cos_distance(pred_y, test_y)
    t_f = samd_d(test_y, pred_y)
    return rmse, l_inf, r_2, rel_1, dist, s_d, t_f, pred_y