########################################################################
## Load steady laminar flamelet data
########################################################################

species_to_remove_list = ['N2', 'AR', 'HE']

data_path = './'
    
STEADY_state_space = pd.read_csv(data_path + 'STEADY-' + data_tag + '-state-space.csv', sep = ',', header=None).to_numpy()
STEADY_state_space_sources = pd.read_csv(data_path + 'STEADY-' + data_tag + '-state-space-sources.csv', sep = ',', header=None).to_numpy()
STEADY_mf = pd.read_csv(data_path + 'STEADY-' + data_tag + '-mixture-fraction.csv', sep = ',', header=None).to_numpy()
STEADY_state_space_names = pd.read_csv(data_path + 'STEADY-' + data_tag + '-state-space-names.csv', sep = ',', header=None).to_numpy().ravel()

for species_to_remove in species_to_remove_list:

    # Remove species:
    (STEADY_species_index, ) = np.where(STEADY_state_space_names==species_to_remove)
    if len(STEADY_species_index) != 0:
        print('Removing ' + STEADY_state_space_names[int(STEADY_species_index)] + '.')
        STEADY_state_space = np.delete(STEADY_state_space, np.s_[STEADY_species_index], axis=1)
        STEADY_state_space_sources = np.delete(STEADY_state_space_sources, np.s_[STEADY_species_index], axis=1)
        STEADY_state_space_names = np.delete(STEADY_state_space_names, np.s_[STEADY_species_index])
    else:
        pass

state_space = STEADY_state_space
state_space_sources = STEADY_state_space_sources
mf = STEADY_mf
state_space_names = STEADY_state_space_names
    
(n_observations, n_variables) = np.shape(state_space)
    
print('\nThe data set has ' + str(n_observations) + ' observations.')