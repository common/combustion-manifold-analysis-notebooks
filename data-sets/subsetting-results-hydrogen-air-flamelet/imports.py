import numpy as np
import pandas as pd
from PCAfold import preprocess
from PCAfold import reduction
from PCAfold import analysis
from PCAfold.styles import *
import plotly as plty
from plotly import express as px
from plotly import graph_objects as go

# Common settings: ------------------------------------------------------------
random_seed = 100
norm = 'cumulative'
penalty_function = 'log-sigma-over-peak'
bandwidth_values = np.logspace(-7, 3, 200)

def plot_3d(x, y, z, color, cmap='plasma', s=2):

    fig = go.Figure(data=[go.Scatter3d(
        x=x.ravel(),
        y=y.ravel(),
        z=z.ravel(),
        mode='markers',
        marker=dict(
            size=s,
            color=color.ravel(),
            colorscale=cmap,
            opacity=1,
            colorbar=dict(thickness=20)
        )
    )])
    
    fig.update_layout(autosize=False,
                width=1000, height=600,
                margin=dict(l=65, r=50, b=65, t=90),
                scene = dict(
                xaxis_title='x',
                yaxis_title='y',
                zaxis_title='z'))
    
    fig.show()