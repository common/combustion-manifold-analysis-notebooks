import plotly as plty
from plotly import express as px
from plotly import graph_objects as go
import plotly.figure_factory as ff
from sklearn.cluster import AgglomerativeClustering
cmap = px.colors.diverging.Spectral

def plot_data_w_regression(transformed_mat, transformed_srcs, dist, n_r, t_f, l = 0):
    """
    dist: cos distance between y_true and y_pred 
    n_r: ((||y_true|| - ||y_pred||)**2)**0.5 / (||y_true|| + epsilon)
    t_f: I(<y_true, y_pred> >= 0)
    l: label of membership of each clusters 
    """
    dist = dist.reshape([dist.shape[0],])
    n_r = n_r.reshape([n_r.shape[0],])
    t_f = t_f.reshape([t_f.shape[0],])
    cmap = px.colors.diverging.Spectral
    if np.sum(l) != 0:
      data = \
        {
          'DML_1':transformed_mat[:,0],
          'DML_2':transformed_mat[:,1],
          'DML_3':transformed_mat[:,2],
          'Src DML_1': transformed_srcs[:,0],
          'Src DML_2': transformed_srcs[:,1],
          'Src DML_3': transformed_srcs[:,2],
          'cos_dist' : dist.reshape([dist.shape[0],]),
          'n_r': n_r, 
          't_f': t_f, 
          'cluster_label': l,
        }
      stuffToPlot = \
        {
          'Src DML_1': 'DML 1 source',
          'Src DML_2': 'DML 2 source',
          'Src DML_3': 'DML 3 source',
          'cos_dist' : 'cos distance',
          'n_r': 'norm ratio',
          't_f': 'true_false',
          'cluster_label' : 'cluster_label',
  
        }
      for fieldName, title in stuffToPlot.items():
            p = \
            px.scatter_3d(data,
                  x='DML_1', 
                  y='DML_2', 
                  z='DML_3', 
                  color=fieldName,
                  title=title,
                  color_continuous_scale=cmap)
            p.show()

    else:
      data = \
      {
          'DML_1':transformed_mat[:,0],
          'DML_2':transformed_mat[:,1],
          'DML_3':transformed_mat[:,2],
          'Src DML_1': transformed_srcs[:,0],
          'Src DML_2': transformed_srcs[:,1],
          'Src DML_3': transformed_srcs[:,2],
          'cos_dist' : dist.reshape([dist.shape[0],]),
          'n_r': n_r, 
          't_f': t_f, 
      }
    
      stuffToPlot = \
      {
          'Src DML_1': 'DML 1 source',
          'Src DML_2': 'DML 2 source',
          'Src DML_3': 'DML 3 source',
          'cos_dist' : 'cos distance',
          'n_r': 'norm ratio',
          't_f': 'true_false',
  
      }
      for fieldName, title in stuffToPlot.items():
        p = \
        px.scatter_3d(data,
                  x='DML_1', 
                  y='DML_2', 
                  z='DML_3', 
                  color=fieldName,
                  title=title,
                  color_continuous_scale=cmap)
        p.show()

def plot_data(transformed_mat, transformed_srcs):
    cmap = px.colors.diverging.Spectral
    data = \
    {
          'DML_1':transformed_mat[:,0],
          'DML_2':transformed_mat[:,1],
          'DML_3':transformed_mat[:,2],
          'Src DML_1': transformed_srcs[:,0],
          'Src DML_2': transformed_srcs[:,1],
          'Src DML_3': transformed_srcs[:,2],
         
    }
    
    stuffToPlot = \
    {
          'Src DML_1': 'DML 1 source',
          'Src DML_2': 'DML 2 source',
          'Src DML_3': 'DML 3 source',
  
    }
    for fieldName, title in stuffToPlot.items():
        p = \
        px.scatter_3d(data,
                  x='DML_1', 
                  y='DML_2', 
                  z='DML_3', 
                  color=fieldName,
                  title=title,
                  color_continuous_scale=cmap)
        p.show()

def plot_3d(x, y, z, color):

    fig = go.Figure(data=[go.Scatter3d(
        x=x.ravel(),
        y=y.ravel(),
        z=z.ravel(),
        mode='markers',
        marker=dict(
            size=2,
            color=color.ravel(),
            colorscale='inferno',
            opacity=0.8
        )
    )])
    
    fig.update_layout(autosize=False,
                width=1000, height=600,
                margin=dict(l=65, r=50, b=65, t=90),
                scene = dict(
                xaxis_title='x',
                yaxis_title='y',
                zaxis_title='z'))
    
    fig.show()
    
def plot_3d_regression(x, y, original, predicted):
    
    fig = go.Figure(data=[go.Scatter3d(
    x=x.ravel(),
    y=y.ravel(),
    z=original.ravel(),
    mode='markers',
    marker=dict(
        size=2,
        color='#aeaeae',
        opacity=1
    )
    ),
    go.Scatter3d(
        x=x.ravel(),
        y=y.ravel(),
        z=predicted.ravel(),
        mode='markers',
        marker=dict(
            size=4,
            color='#ff928b',
            opacity=0.5
        )
    )])
    
    fig.update_layout(autosize=False,
                width=1000, height=600,
                margin=dict(l=65, r=50, b=65, t=90),
                scene = dict(
                xaxis_title='x',
                yaxis_title='y',
                zaxis_title='Phi'))
    
    fig.show()

def short_diagnostic(src_path, state_path,
                     ncomp, scaling, r_b_dml, r_b_pca,
                     alpha = 1e-15, l = 1, sigma = 1e-5, bw = 1, 
                     random_seed = 42, p1 = 0.1, p2 = 0.1, n_cluster = 6,
                     m1_save_path = None, nw = True, weight_option = "Else",
                     m1_path = None, vis = False, plot_error = False, plot_pca = False, 
                     regress_by_cluster = False, double_dimension = False):
    """
    no cross-validation result anymore 
    only do train/test split 
    report r_square, percentage of same_direction 
    src_path: file path of source term data
    state_path: file path of state term data
    ncomp: dimension/number of components in low dimension
    scaling: the scaling choose by user for DML matrix  
    r_b_dml: the bandwidth used in kernel-regression for dml-transformed data
    r_b_pca: the bandwidth used in kernel-regression for pca-transformed data
    alpha: the coefficent of penalty for kernel-ridge regression
    gamma: the pulling parameter in nw-estimator, for origial kernel ridge regression, gamma = 0 
    l: the scalar used in adjusting the strength of normalized weight vector in learning dml transformation, 
        see optimize_dml for details
    sigma: the tuning parameter in learn dml matrix 
    nw: using the NW estimator's type kernel regression or not. 
    random_seed: the seed used in train/test split 
    p1: percentile of data used in learned dml matrix 
    p2: percentile of data used in training kernel regression model 
    n_cluster: number of cluster in learning dml transformation 
    m1_save_path: the path to save the learned m1 matrix 
    m1_path: if not None then directly load m1 matrix 
    weight_option: the weight option used in learned m1 matrix, default is Linear. 
    vis: Default is False, if True, the DML manifold will be drawn and displayed and the m1 matrix will be returned, regression result will not be calculated.
    plot_error: boolean to determine if the regression error should be plotted or not 
    plot_pca: if pca result should be plotted or not 
    regress_by_cluster: if the train set for transformed mat should be sampled from clustering or sampled by random choice
    default clustering here is AgglomerativeClustering but kmean can also be used here... (need to modify source code)
    double_dimension: if the scaled unit vector will be appended after transformed matrix (X) or not 
    """
    X_scaled, S_X_scaled = preprocess_data(src_path, state_path, ncomp, scaling, False)
    np.random.seed(random_seed) 
    def my_func_1(a):
        if np.linalg.norm(a) != 0:
            return a / np.linalg.norm(a)
        return a
    if m1_path == None:
        """
        THEN no pretrained dml matrix avaliable 
        visulation is automatically set as true
        """
        vis = True
        SX_scaled_new = np.apply_along_axis(my_func_1,1,S_X_scaled)
        m1 = optimize_dml(X = S_X_scaled, W_X = np.hstack((S_X_scaled,SX_scaled_new * l)), 
                          b = bw, projection_dimension=ncomp, sigma=sigma, column_weight=None, 
                          weight_option="Else", percentile=p1, sampling_by_cluster=True, n_clusters=n_cluster)
        np.savetxt(m1_save_path, np.real(m1),delimiter=",")
        print(m1_save_path)
    else:
        m1 = np.loadtxt(m1_path,delimiter=",")
    
    transformed_srcs = S_X_scaled @ np.real(m1)
    transformed_srcs.astype(float)
    if double_dimension == True:
        transformed_mat_1 = X_scaled @ np.real(m1)
        new_mat = np.apply_along_axis(my_func_1,1,transformed_mat_1) 
        transformed_mat = np.hstack((transformed_mat_1,new_mat * l))
    else:
        transformed_mat = X_scaled @ np.real(m1) 
    transformed_mat.astype(float)
    if X_scaled.shape[0] > 30000:
        vis = False
    if vis == True:
        plot_data(transformed_mat, transformed_srcs)
        return m1 
    if regress_by_cluster == False:
        n = transformed_mat.shape[0]
        index = np.random.choice(n, size = int(n * p2), replace= False)
        X_1 = transformed_mat[index,:]
        Y_1 = transformed_srcs[index, :]
        X_2 = np.delete(transformed_mat, index, axis = 0)
        Y_2 = np.delete(transformed_srcs, index, axis = 0)
        rmse, l_inf, r_2, rel_1, dist, same_direction, t_f, pred_y = run_kernel_regression(X_1, Y_1, X_2, Y_2, r_b_dml, alpha, nw)
    else:
        km =  AgglomerativeClustering(n_clusters=n_cluster).fit(transformed_mat)
        label = km.labels_
        X_1, X_2, Y_1, Y_2 = train_test_split(transformed_mat, transformed_srcs, test_size = p2, random_state = 42, stratify = label)
        rmse, l_inf, r_2, rel_1, dist, same_direction, t_f, pred_y = run_kernel_regression(X_1, Y_1, X_2, Y_2, r_b_dml, alpha, nw)

    if nw == False:
        print("Regression|" + str(scaling) + "|scaling|DML|KRR")
    else:
        print("Regression|" + str(scaling) + "|scaling|DML|NW-KR")
    
    epsilons = [1e-4, 1e-5, 1e-6, 1e-7, 1e-8]
    print("rsq is : "  + str(np.round(r_2,6))) ##  revise float format print(... )
    print("rmse/max is : " + str(np.round(np.array(rmse)/np.array(l_inf),6)))  #revise float format print(... )
    np.savetxt("dml_r2.csv",np.array(r_2),delimiter=",")
    print("% same direction: {:0.6f}".format(same_direction))
    for e in epsilons:
        print("epsilon is :" + str(e))
        _, avg_n_r = norm_ratio(Y_2, pred_y, e)
        print('average of relative error is : {:0.6f}'.format(avg_n_r))
    
    print("\n")

    if plot_error == True:
        _, _, _, _, dist, _, t_f, pred_y = run_kernel_regression(X_1, Y_1, transformed_mat, transformed_srcs, r_b_dml, alpha, nw)
        n_r, _ = norm_ratio(transformed_srcs, pred_y , epsilons[-1])
        plot_data_w_regression(transformed_mat, transformed_srcs, dist, n_r, t_f, label)

    if plot_pca == False:
        return m1 

    Scalings = ["vast","auto", "pareto"]
    for s in Scalings:
        pca_x, pca_s_x = preprocess_data(src_p, state_p, ncomp, s, True)  
        pca_x.astype(float)
        pca_s_x.astype(float) 
        X_1 = pca_x[index,:]
        Y_1 = pca_s_x[index, :]
        X_2 = np.delete(pca_x, index, axis = 0)
        Y_2 = np.delete(pca_s_x, index, axis = 0)
        rmse, l_inf, r_2, rel_1, dist, same_direction, t_f, _ = run_kernel_regression(X_1, Y_1, X_2, Y_2, r_b_pca, alpha, nw)
        if nw == False:
          print("Regression|" + str(s) + "|scaling|PCA|KRR")
        else:
          print("Regression|" + str(s) + "|scaling|PCA|NW-KR")
        print("rsq is : "  + str(np.round(r_2,6))) ##  revise float format print(... )
        print("rmse/max is : " + str(np.round(np.array(rmse)/np.array(l_inf),6))) 
        print("% same direction: {:0.6f}".format(same_direction))
        if plot_pca == True:
            _, _, _, _, dist, _, t_f, pred_y = run_kernel_regression(X_1, Y_1, pca_x, pca_s_x, r_b_pca, alpha, nw)
            n_r, _ = norm_ratio(pca_s_x, pred_y , epsilons[-1])
            plot_data_w_regression(pca_x, pca_s_x, dist, n_r, t_f)

    return m1 