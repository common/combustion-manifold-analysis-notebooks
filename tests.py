import unittest
import numpy as np
from regression import *
from sklearn.metrics import r2_score

class DML(unittest.TestCase):

    def test__DML__allowed_calls(self):

        try:
            print('DML')
        except Exception:
            self.assertTrue(False)

class Regression(unittest.TestCase):

    def test__calculate_r2_versus_sklearn_metrics_r2_score(self):

        tol = np.finfo(float).eps

        for i in range(0,100):
            X = np.random.rand(100,2)
            (r2, _) = calculate_r2(X[:,0], X[:,1])
            r2_sklearn = r2_score(X[:,0], X[:,1])

            if abs(r2 - r2_sklearn) > tol:
                self.assertTrue(False)

    def test__knn_gpr_average__not_allowed_calls(self):

        X = np.zeros((100,5))
        Y = np.zeros((100,10))
        Query_Point = np.zeros((20,2))
        Query_Point_y = np.zeros((20,10))

        with self.assertRaises(ValueError):
            pred = knn_gpr_average(X, Y, 10, Query_Point, Query_Point_y, return_error=False)

        X = np.zeros((100,5))
        Y = np.zeros((100,10))
        Query_Point = np.zeros((20,5))
        Query_Point_y = np.zeros((20,2))

        with self.assertRaises(ValueError):
            pred = knn_gpr_average(X, Y, 10, Query_Point, Query_Point_y, return_error=False)

        X = np.zeros((100,5))
        Y = np.zeros((100,10))
        Query_Point = np.zeros((20,5))
        Query_Point_y = np.zeros((30,10))

        with self.assertRaises(ValueError):
            pred = knn_gpr_average(X, Y, 10, Query_Point, Query_Point_y, return_error=False)

        X = np.zeros((100,5))
        Y = np.zeros((200,10))
        Query_Point = np.zeros((20,5))
        Query_Point_y = np.zeros((20,10))

        with self.assertRaises(ValueError):
            pred = knn_gpr_average(X, Y, 10, Query_Point, Query_Point_y, return_error=False)

    def test__knn_kreg_average__not_allowed_calls(self):

        X = np.zeros((100,5))
        Y = np.zeros((100,10))
        Query_Point = np.zeros((20,2))
        Query_Point_y = np.zeros((20,10))

        with self.assertRaises(ValueError):
            pred = knn_kreg_average(X, Y, 10, Query_Point, Query_Point_y, return_error=False)

        X = np.zeros((100,5))
        Y = np.zeros((100,10))
        Query_Point = np.zeros((20,5))
        Query_Point_y = np.zeros((20,2))

        with self.assertRaises(ValueError):
            pred = knn_kreg_average(X, Y, 10, Query_Point, Query_Point_y, return_error=False)

        X = np.zeros((100,5))
        Y = np.zeros((100,10))
        Query_Point = np.zeros((20,5))
        Query_Point_y = np.zeros((30,10))

        with self.assertRaises(ValueError):
            pred = knn_kreg_average(X, Y, 10, Query_Point, Query_Point_y, return_error=False)

        X = np.zeros((100,5))
        Y = np.zeros((200,10))
        Query_Point = np.zeros((20,5))
        Query_Point_y = np.zeros((20,10))

        with self.assertRaises(ValueError):
            pred = knn_kreg_average(X, Y, 10, Query_Point, Query_Point_y, return_error=False)

    def test__singular_kernel_average__not_allowed_calls(self):

        X = np.zeros((100,5))
        Y = np.zeros((100,10))
        Query_Point = np.zeros((20,2))
        Query_Point_y = np.zeros((20,10))

        with self.assertRaises(ValueError):
            pred = singular_kernel_average(X, Y, 10, singular_kernel, Query_Point, Query_Point_y, return_error=False)

        X = np.zeros((100,5))
        Y = np.zeros((100,10))
        Query_Point = np.zeros((20,5))
        Query_Point_y = np.zeros((20,2))

        with self.assertRaises(ValueError):
            pred = singular_kernel_average(X, Y, 10, singular_kernel, Query_Point, Query_Point_y, return_error=False)

        X = np.zeros((100,5))
        Y = np.zeros((100,10))
        Query_Point = np.zeros((20,5))
        Query_Point_y = np.zeros((30,10))

        with self.assertRaises(ValueError):
            pred = singular_kernel_average(X, Y, 10, singular_kernel, Query_Point, Query_Point_y, return_error=False)

        X = np.zeros((100,5))
        Y = np.zeros((200,10))
        Query_Point = np.zeros((20,5))
        Query_Point_y = np.zeros((20,10))

        with self.assertRaises(ValueError):
            pred = singular_kernel_average(X, Y, 10, singular_kernel, Query_Point, Query_Point_y, return_error=False)
