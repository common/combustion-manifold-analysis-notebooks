{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## \"state space\" data generation\n",
    "\n",
    "We generate data by solving the flamelet PDEs:\n",
    "\n",
    "\\begin{equation} \\label{eq:flamelet}\n",
    "\\frac{\\partial \\boldsymbol{\\phi}}{\\partial t} = \\frac{\\chi(Z)}{2}\\frac{\\partial^2 \\boldsymbol{\\phi}}{\\partial Z^2} + \\boldsymbol{S}_{\\phi}(\\boldsymbol{\\phi})\n",
    "\\end{equation}\n",
    "\n",
    "where\n",
    "\n",
    "\\begin{equation} \\label{eq:chi}\n",
    "\\chi(Z) = \\chi_\\mathrm{max}\\mathrm{exp}\\left(-2\\left[ \\mathrm{erf^{-1}}\\left(2Z-1\\right) \\right]^2\\right)\n",
    "\\end{equation}\n",
    "\n",
    "Equation \\eqref{eq:flamelet} is parameterized by the mixture fraction, $Z$, and a maximum scalar dissipation rate, $\\chi_\\mathrm{max}$. We typically choose a grid for $Z$, which exists between [0,1], and for each value in a range of $\\chi_\\mathrm{max}$ solve for the steady-state solution ($\\frac{\\partial \\boldsymbol{\\phi}}{\\partial t}=0$) of $\\boldsymbol{\\phi}$ at each grid point.\n",
    "\n",
    "We refer to $\\boldsymbol{\\phi}$ as the state variables. This is a vector of $n_\\phi$ variables including temperature and mass fractions of chemical species involved in reactions. $\\boldsymbol{S}_{\\phi}(\\boldsymbol{\\phi})$ are the chemical source terms (or state space sources) which are complicated, nonlinear functions of all state variables $\\boldsymbol{\\phi}$, and describe changes in $\\boldsymbol{\\phi}$ due to chemical reactions.\n",
    "\n",
    "The state space dimensionality, $n_\\phi$, is determined by the complexity of the chemistry. Two example datasets we have been working with are labeled with H$_2$ and CH$_4$, which are the chemical formulas for hydrogen and methane, respectively. There is an example below for how to see the dimensionality of the state space data. The variable names associated with each dimension include temperature (T) and the chemical formulas of species involved in the reactions.\n",
    "\n",
    "Our focus is modeling source terms, which means that although $\\boldsymbol{\\phi}$ are the dependent variables of \\eqref{eq:flamelet}, they become the independent variables for our purposes which represent an $n_\\phi$-dimensional manifold. $\\boldsymbol{S}_{\\phi}(\\boldsymbol{\\phi})$ become the dependent or response variables that exist over the manifold defined by $\\boldsymbol{\\phi}$.\n",
    "\n",
    "Our goal is to find a linear projection/truncation that reduces the dimensionality of $\\boldsymbol{\\phi}$ (and therefore $\\boldsymbol{S}_{\\phi}(\\boldsymbol{\\phi})$ as well) so that we may solve transport equations for the projected variables over a lower-dimensional manifold.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### example loading data\n",
    "\n",
    "Here is the most recent code that exists in the notebooks we have been sharing for loading data. I've included comments and print statements to explain how each variable relates to those above.\n",
    "\n",
    "One subtlety is we save the stoichiometric dissipation rate $\\chi_{st}$ instead of the maximum, $\\chi_\\mathrm{max}$. These can be related using \\eqref{eq:chi} where $\\chi_{st}$ = $\\chi(Z_{st})$ and we can get $Z_{st}$ from the chemistry if/when needed.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "number of observations or solutions in state space: 13650\n",
      "dimensionality of state space: 12\n",
      "names of state space dimensions:\n",
      " ['T' 'H' 'H2' 'O' 'OH' 'H2O' 'O2' 'HO2' 'H2O2' 'N2' 'AR' 'HE']\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "\n",
    "# this is the folder and prefix for loading the hydrogen data (H2)\n",
    "folder = '../data-sets/subsetting-results-hydrogen-air-flamelet/'\n",
    "prefix = 'STEADY-clustered-flamelet-H2'\n",
    "\n",
    "# solutions to the flamelet equations, size n_obs x n_phi, number of columns gives state space dimensionality\n",
    "# each row is an observation of the state space vector, phi\n",
    "state_space = np.loadtxt(folder+prefix+'-state-space.csv', delimiter = ',')\n",
    "\n",
    "print('number of observations or solutions in state space:', state_space.shape[0])\n",
    "print('dimensionality of state space:', state_space.shape[1])\n",
    "\n",
    "# the state space source terms, also size n_obs x n_phi\n",
    "state_space_sources = np.loadtxt(folder+prefix+'-state-space-sources.csv', delimiter = ',')\n",
    "\n",
    "# the names of each state space dimension or variable\n",
    "# T represents temperature and the rest represent chemical formulas\n",
    "state_space_names_raw = pd.read_csv(folder+prefix+'-state-space-names.csv',header=None)\n",
    "state_space_names = np.array([x for x in state_space_names_raw[0]])\n",
    "print('names of state space dimensions:\\n',state_space_names)\n",
    "\n",
    "# the mixture fraction value for each observation of state_space\n",
    "STEADY_mf = np.loadtxt(folder+prefix+'-mixture-fraction.csv', delimiter=',')\n",
    "\n",
    "# the stoichiometric dissipation rate values for each observation of state_space\n",
    "STEADY_chi = np.loadtxt(folder+prefix+'-dissipation-rates.csv', delimiter=',')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## low-dimensional projection\n",
    "\n",
    "We have mainly focused on using Principal Component Analysis (PCA) to reduce the dimensionality of our state space. Since this is a data-driven technique, we first gather many observations of the state variables we want to project to a lower dimensional space. We compose a matrix $[\\phi]$ whose rows represent the many observations or solutions we have to \\eqref{eq:flamelet} and whose columns represent the state variables. This matrix is of size $n_{obs}$ by $n_\\phi$.\n",
    "\n",
    "PCA then defines a linear projection operator, $[A]$, to transform state space observations $[\\phi]$ into principal component (PC) space\n",
    "\n",
    "\\begin{equation}\n",
    "[\\eta] = ([\\phi] - [C])[D^{-1}][A]\n",
    "\\end{equation}\n",
    "\n",
    "where $[C]$ centers the observations of $[\\phi]$ around the average values and $[D^{-1}]$ scales them.\n",
    "We choose a dimensionality for the projection, $n_\\eta$, where $n_\\eta < n_\\phi$, which defines the shape of $[A]$ as $n_\\phi$ by $n_\\eta$. The manifold defined by $\\boldsymbol{\\phi}$ is then projected to a lower-dimensional manifold defined by $\\boldsymbol{\\eta}$.\n",
    "\n",
    "We can then apply this transformation to a set of equations for $\\boldsymbol{\\phi}$ to get the corresponding equations for $\\boldsymbol{\\eta}$. As an example, we can transform the flamelet equations:\n",
    "\n",
    "\\begin{equation} \\label{eq:eta}\n",
    "\\frac{\\partial \\boldsymbol{\\eta}}{\\partial t} = \\frac{\\partial \\boldsymbol{\\phi}}{\\partial t}[D^{-1}][A] = \\frac{\\chi(Z)}{2}\\frac{\\partial^2 \\boldsymbol{\\eta}}{\\partial Z^2} + \\boldsymbol{S}_{\\eta}\n",
    "\\end{equation}\n",
    "\n",
    "where \n",
    "\n",
    "\\begin{equation}\n",
    "\\boldsymbol{S}_{\\eta} = \\boldsymbol{S}_{\\phi}[D^{-1}][A]\n",
    "\\end{equation}\n",
    "\n",
    "Notice that centering is applied in going from $\\boldsymbol{\\phi}$ to $\\boldsymbol{\\eta}$ but not in going from $\\boldsymbol{S}_\\phi$ to $\\boldsymbol{S}_\\eta$. An example of the code for this transformation is shown below.\n",
    "\n",
    "In order to solve \\eqref{eq:eta} numerically, we need to have a model for evaluating $\\boldsymbol{S}_\\eta(\\boldsymbol{\\eta})$.\n",
    "\n",
    "We have been working on regressing our observed $[{S}_\\eta] = [{S}_{\\phi}][D^{-1}][A]$ over the observed $[\\eta] = ([\\phi] - [C])[D^{-1}][A]$ to get $\\boldsymbol{S}_\\eta(\\boldsymbol{\\eta})$. Another, likely more expensive and more error prone, option would be to train a model for $\\boldsymbol{\\phi}(\\boldsymbol{\\eta})$ so that $\\boldsymbol{S}_\\eta(\\boldsymbol{\\eta})$ looks like $\\boldsymbol{\\eta} \\rightarrow \\boldsymbol{\\phi} \\rightarrow \\boldsymbol{S}_\\phi \\rightarrow \\boldsymbol{S}_\\eta$. But in both cases, the independent variables/inputs are $\\boldsymbol{\\eta}$ and the dependent variables/outputs/reponse variables are $\\boldsymbol{S}_\\eta$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### example projection of state space and state space source terms to PC space"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "subset of dimensions of state space used in training PCA model: ['T' 'H' 'H2' 'O' 'OH' 'O2']\n"
     ]
    }
   ],
   "source": [
    "from PCAfold import reduction\n",
    "\n",
    "ncomp = 3 # dimensionality of the projected space\n",
    "scaling = 'auto' # scaling method that defines [D^-1]\n",
    "\n",
    "# we've been exploring benefits of subsampling the state space before training a PCA model\n",
    "selected_variables = [0, 1, 2, 3, 4, 6] # indices of state space dimensions to keep\n",
    "print('subset of dimensions of state space used in training PCA model:', state_space_names[selected_variables])\n",
    "phi = state_space[:,selected_variables]\n",
    "S_phi = state_space_sources[:,selected_variables]\n",
    "\n",
    "# define [A]\n",
    "pcaobj = reduction.PCA(phi, scaling=scaling, n_components=ncomp)\n",
    "\n",
    "# transform state space (phi) to PC space (eta)\n",
    "eta = pcaobj.transform(phi)\n",
    "\n",
    "# apply transformation to the state space sources as well to get PC source terms (S_eta)\n",
    "# note these are not centered\n",
    "S_eta = pcaobj.transform(S_phi, nocenter=True)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Discussion in context of using neural networks to solve PDEs\n",
    "\n",
    "In general, the PDEs we are interested in solving are more complicated that the flamelet equations in \\eqref{eq:flamelet}. We have been attempting to solve the transformed flamelet equations in \\eqref{eq:eta} only as a test problem before trying something more complicated. This is why we are attempting to model $\\boldsymbol{S}_\\eta(\\boldsymbol{\\eta})$ so that the model can be used in a variety of PDEs and is not limited to flamelet problems. Solving the flamelet equations offers a computationally cheap way of generating data for training models to be used in simulations that are computationally expensive.\n",
    "\n",
    "So while the method of training an ANN to solve \\eqref{eq:eta} in particular is not necessarily interesting, it does present interesting ideas for augmenting the loss function of regressing $\\boldsymbol{S}_\\eta(\\boldsymbol{\\eta})$. The source terms we transform to PC space, $\\boldsymbol{S}_\\eta$, should satisfy the steady state version of \\eqref{eq:eta}. This means \n",
    "\n",
    "\\begin{equation}\n",
    "\\boldsymbol{S}_\\eta(\\boldsymbol{\\eta}) + \\frac{\\chi(Z)}{2}\\frac{\\partial^2 \\boldsymbol{\\eta}}{\\partial Z^2} = 0\n",
    "\\end{equation}\n",
    "\n",
    "By incorporating this term into the loss function for $\\boldsymbol{S}_\\eta(\\boldsymbol{\\eta})$, we could (depending on model accuracy and smoothness) guarantee the source term model satisfies the flamelet equations and still be able to insert the model into a different PDE system. This presents something to explore.\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.10"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
