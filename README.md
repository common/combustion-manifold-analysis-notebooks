# Combustion manifold analysis

## Python scripts

- DML: [`dml.py`](dml.py)
- Regression: [`regression.py`](regression.py)
- Diagnostics: [`diagnostic.py`](diagnostic.py)

Here's how the scripts can be loaded and the data set path can be setup at the beginnig of each Jupyter notebook:

```python
%run -i ../dml.py
%run -i ../diagnostic.py
%run -i ../regression.py

data_path = '../data-sets/flamelet_data_varying_complexity/'
```

### Singular kernels implemented in `regression.py` as per [1]

![Screenshot](singular-kernels.png)

### Preliminary tests

To run our preliminary tests of the DML functionalities from the `tests.py` script,
execute:

```
python -m unittest discover
```

in the project directory.

## Data sets

- Flamelets with varying complexity [data sets](data-sets/flamelet_data_varying_complexity/)

## Jupyter notebooks

- Generate flamelet data sets [`</>` notebook](notebooks/generate-flamelet-data.ipynb)
- Flamelet manifold example [`</>` notebook](notebooks/flamelet-manifold-example.ipynb)
- Clustering-sampling example [`</>` notebook](notebooks/H2-air-flamelet-clustering-sampling.ipynb)
- Testing variable weighting for DML using the `column_weights` parameter [`</>` notebook](notebooks/testing-variable-weighting-for-DML.ipynb)
- Basic DML example [`</>` notebook](notebooks/dml-basic-example.ipynb)
- Basic DML example with regression [`</>` notebook](notebooks/dml-regression-example.ipynb)
- DML with stratified sampling [`</>` notebook](notebooks/dml_stratify_sampling.ipynb)
- Testing DML with kernel regression using various singular kernels [`</>` notebook](notebooks/testing-DML-with-singular-kernel-regression.ipynb)

## Literature

- [1] [Mikhail Belkin, Alexander Rakhlin, Alexandre B. Tsybakov, *Does data interpolation contradict statistical optimality?*, 2018](https://arxiv.org/abs/1806.09471)