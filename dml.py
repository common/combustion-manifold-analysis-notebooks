import math
import pickle
import itertools
from itertools import combinations
import numba
from tqdm import tqdm
from numba import jit,generated_jit
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
from scipy.linalg import sqrtm
from sklearn import preprocessing
from sklearn.cluster import AgglomerativeClustering
from PCAfold import PCA
from PCAfold import preprocess
from PCAfold import reduction
from PCAfold import analysis
from PCAfold import KReg
import matplotlib.pyplot as plt
#############################################################################################################



def preprocess_data(src_path, state_path, ncomp, scaling, pca_or_not):
    """
    function to preprocess data 
    if pca_or_not = True, then return the data after pca transformation 
    else return the data after scaling, where scaling is determined by scaling parameter. 
    
    src_path: the file path of source term 
    state_path: the file path of state variable 
    ncomp: number of component
    scaling: the scaling you want to choose 
    to learn dml transformation, we want pca_or_not = False
    """
    X_dict = pickle.load(open(state_path         ,'rb'))
    S_dict = pickle.load(open(src_path ,'rb'))
    nthSpecies = 'N2'
    exclude = [nthSpecies]
    varNames_full = [name for name in S_dict.keys() if name not in exclude] 
    X   = np.hstack([X_dict[name][None].T for name in varNames_full])
    S_X = np.hstack([S_dict[name][None].T for name in varNames_full])
    X, i_removed, i_retained = preprocess.remove_constant_vars(X)
    S_X = S_X[:,i_retained]
    varNames = list(np.array(varNames_full)[i_retained])
    pcaObj = reduction.PCA(X, scaling=scaling, n_components=ncomp)
    if pca_or_not:
       pca_x = pcaObj.transform(X)
       pca_s_x = pcaObj.transform(S_X, nocenter = True)
       return pca_x, pca_s_x
    X_scaled = np.zeros_like(X)
    S_X_scaled = np.zeros_like(X)
    print(varNames)
    for i in range(X.shape[1]):
      X_scaled[:,i] = (X[:,i] - pcaObj.X_center[i])/pcaObj.X_scale[i]
      S_X_scaled[:,i] = S_X[:,i]/pcaObj.X_scale[i]
    return X_scaled, S_X_scaled

"""
function to determine number of cluster by Kmean 
"""

def n_cluster(max_number, S_X_scaled):
    Sum_of_squared_distances = []
    K = range(1,max_number)
    for k in K:
        km = KMeans(n_clusters=k)
        km = km.fit(S_X_scaled)
        Sum_of_squared_distances.append(km.inertia_)
    plt.plot(K, Sum_of_squared_distances, 'bx-')
    plt.xlabel('The number of cluster')
    plt.ylabel('Inertia')
    plt.title('Elbow Method For Optimal k')
    plt.show()


################################################################################################################### 
"""
below are functions for DML algorithm
"""

def create_sub_set(nums):
    return np.array([list(x) for x in combinations(nums,2)])


# The function to calculate outer product between i_th and j_th observation in X
@jit(nopython = True)
def calculate_outer_product(X,i,j):
    return np.outer((X[i,:] - X[j, :]), (X[i,:] - X[j, :]))

@jit(nopython = True)
def change_to_psd(m, gamma):
    """
    m: the matrix you want to change to psd matrix 
    gamma: threshold for minimum eigenvalue, must be alway greater than 0
    """
    value, vector = np.linalg.eig(m)
    value = np.real(value)
    vector = np.real(vector)
    value = np.maximum(value, gamma * np.ones(len(value)))
    psd_matrix = vector @ np.diag(value) @ np.linalg.inv(vector)
    return psd_matrix

# The function to calculate H matrix in step 1 
@jit(nopython = True)
def calculate_H(X, close_set, delta):
    n, d = X.shape
    result = np.zeros(shape = (d, d))
    for l in range(len(close_set)):
        i = close_set[l][0]
        j = close_set[l][1]
        result +=  calculate_outer_product(X,i,j)
    psd_mat = change_to_psd(result, delta)
    return psd_mat 


# The function to calculate gradient G matrix in step 3 
# H^{-1/2} is actually calculated by cholesky decompostion of H = LL^{*} and take inverse mutiply (X[i]-X[j]), the detail can be seen in "Distance Metric Learning with Eigenvalue Optimization"
@jit(nopython=True)
def calculate_g_delta(X, W_X, far_set, sigma, M, H, b, weight_option, verbose=False):
    """
    X: raw data used in learning DML matrix 
    W_X: weight matrix used to determine the closeness of data points in X 
    sigma: tuning parameter in calculated g_delta
    far_set: list of lists to denote which two points are "far" away from each other, details seen in "Distance Metric Learning with Eigenvalue Optimization"
    M: intermediate result of DML matrix from last iteration 
    H: details seen in "Distance Metric Learning with Eigenvalue Optimization"
    weight_option: Three types of weight function can be chosen: "Gaussian", "Laplacian", "Inverse", if not specified, default is "Inverse"
    b: bandwidth in calculation of weight function.
    """
    n, d = X.shape
    numerator = np.zeros(shape =(d,d))
    denominator = np.zeros(shape = (d,d))
    L = np.linalg.cholesky(H)
    H_inverse_half  = np.linalg.inv(L)
    if weight_option == "Gaussian":
      if verbose: print("True")
      for l in range(len(far_set)):
          i = far_set[l][0]
          j = far_set[l][1]
          x_gamma_hat = H_inverse_half @ (X[i,:] - X[j, :]).reshape((d,1)) @ (X[i,:] - X[j, :]).reshape((1,d)) @ H_inverse_half.T * (1 - np.exp(-np.linalg.norm(W_X[i,:] - W_X[j,:]) / b**2))
          numerator += np.exp(-(np.trace(x_gamma_hat.T @ M) / sigma)) * x_gamma_hat  
          denominator += np.exp(-(np.trace(x_gamma_hat.T @ M) / sigma))
    elif weight_option == "Laplacian":
      for l in range(len(far_set)):
          i = far_set[l][0]
          j = far_set[l][1]
          x_gamma_hat = H_inverse_half @ (X[i,:] - X[j, :]).reshape((d,1)) @ (X[i,:] - X[j, :]).reshape((1,d)) @ H_inverse_half.T * (1 - np.exp(-np.linalg.norm(W_X[i,:] - W_X[j,:]) / b))
          numerator += np.exp(-(np.trace(x_gamma_hat.T @ M) / sigma)) * x_gamma_hat  
          denominator += np.exp(-(np.trace(x_gamma_hat.T @ M) / sigma))
    else:
      for l in range(len(far_set)):
          i = far_set[l][0]
          j = far_set[l][1]
          x_gamma_hat = H_inverse_half @ (X[i,:] - X[j, :]).reshape((d,1)) @ (X[i,:] - X[j, :]).reshape((1,d)) @ H_inverse_half.T * (1 / max(1e-8, np.sum((W_X[i,:] - W_X[j,:])**2)**0.25))
          numerator += np.exp(-(np.trace(x_gamma_hat.T @ M) / sigma)) * x_gamma_hat  
          denominator += np.exp(-(np.trace(x_gamma_hat.T @ M) / sigma))
 
    q = numerator / denominator 
    return q 

def optimize_dml(X, W_X, b, projection_dimension, sigma, epoch = 100, column_weight = None, weight_option = None, percentile = 0.05, standardize = True, sampling_by_cluster = True, n_clusters = 2, verbose=False):
    """
    Input arguments: 
    X: data used to learn the transformation, usually a certain percentile, say 0.05 would be extracted from this X to learn the transformation
    W_X: data used in weight function 
    b: the bandwidth in kernel weight or laplace weight function, only triggered when weight function is "Gaussian" or "Laplacian"
    projection_dimension: The number of projection dimensions used for the transformation. 
    sigma: the main tuning/smoothing parameter in dml algorithm, used to calculated gradient. 
    epoch: the iterations for the algorithm - 1
    weight_option: Three types of weight function can be chosen: "Gaussian", "Laplacian", "Inverse", if not specified, default is "Inverse"
    percentile: the percentile of the data used to learn transformation, default is 0.05 
    standardize: standardize data or not before learning transformation, default is True (so scaling data will be standardized again)
    sampling_by_cluster: default is True, set True will pick the training data of dml by clustering (recommended)
    n_cluster: if sampling_by_cluster is True, then determine how many cluster will be used in clustering (for stratify sampling)
    column_weight: default is None, mean column has same weight, else should be list of positive number, the length must be equal to number of column

    Return: 
    m: the transformation matrix learnt by algorithm, the dimension will be (projection_dimension, projection_dimension)
    """
    # hardcode parameters 
    n, d = X.shape
    stop_tolerance = 1e-5
    delta = 1e-5
    smooth_parameter = d * sigma

    # create the subset used in learning the transformation, based on the X and Percentile
    if sampling_by_cluster == False:
        X = X[np.arange(0, n, int(1/percentile)), :] 
        W_X = W_X[np.arange(0, n, int(1/percentile)), :]
    else:
        #@jit(nopython=False)
        km =  KMeans(n_clusters=n_clusters, random_state=42).fit(X)
        l = km.labels_
        X = np.hstack((X,np.arange(X.shape[0]).reshape(X.shape[0],1)))
        _, X, _, _ = train_test_split(X, l, test_size = percentile, random_state = 42, stratify = l)
        W_X = W_X[[int(s) for s in X[:,-1]],:]
        X = X[:, :X.shape[1]-1]

    #@jit(nopython=False)
    if verbose: print(X.shape[0])
    # create the far_set and close_set in data to represent pairwise similiarity 
    # currently for our algorithm, all the pairs are close_set and all the pairs are far_set. 
    close_set = create_sub_set(np.arange(X.shape[0]))
    # far_set = np.copy(close_set)
    if verbose: print("subset_done")
    # determine if the data should be standardize or not, means (x - mean(x))/sigma(x)
    if standardize == True:
        X = preprocessing.scale(X)
        #W_X = preprocessing.scale(X)

    # determine if the user-input of column weight will be applied or not. 
    if column_weight != None:
        if len(column_weight) != d:
            print("The column_weight input is incorrect \n")
            print("use default weight")
        else:
            X = X @ np.diag(column_weight)

    # calculate H matrix as in step 1 described in above cell 
    
    H = calculate_H(X, close_set, delta)
    if verbose: print(np.trace(H))
    # initialize the M matrix M_0
    M = np.eye(d)
    err_buff = [] #... 
    # below is the implementation of algorithm shown in the description above this cell
    for i in tqdm(range(1, epoch)):
        # calculate gradient matrix G now use close_set since close_set =\= far_set 
        g = calculate_g_delta(X, W_X, close_set, smooth_parameter, M, H, b, weight_option, verbose=verbose)
        # find max eigenvector v_t 
        w,v = np.linalg.eig(g) 
        index_max = np.argmax(w)
        v_t = np.real(v[:, index_max])

        # update the matrix M 
        M_new = (i - 1) / i * M + (d / i) * np.outer(v_t, v_t) 
        err = (np.sum(M_new - M))**2
        if verbose:
            print("\n")
            print("erros is ", err)
            print("\n")
        if len(err_buff) == 3:
            err_buff.pop(0)
            err_buff.append(err)
        else:
            err_buff.append(err)
        if len(err_buff) == 3 and np.mean(err_buff) <= stop_tolerance:
            print("A")
            M = M_new
            break
        else:
            M = M_new 

    # pick the top k eigenvector from M and return. 
    val, vec = np.linalg.eig(M)
    top_k_index = val.argsort()[-projection_dimension:][::-1]
    return vec[:, np.array(top_k_index)]

"""
Example:
bw = 1e-3
def my_func_1(a):
    return a / np.linalg.norm(a)
SX_scaled_new = np.apply_along_axis(my_func_1,1,S_X_scaled)
l = 20
m = optimize_dml(X = S_X_scaled, W_X = np.hstack((S_X_scaled,SX_scaled_new * l)), b = bw, projection_dimension= 4, sigma = 1e-7, column_weight=None, weight_option="ELSE", percentile=0.2, sampling_by_cluster=True, n_clusters=4)
m = np.real(m)
wx = S_X_scaled @ m 
m1 = optimize_dml(X = S_X_scaled, W_X = wx, b = bw, projection_dimension=3, sigma=1e-7, column_weight=None, weight_option="ELSE", percentile=0.2, sampling_by_cluster=True, n_clusters=4)
np.savetxt("m_1e-7_linear_2.csv", m, delimiter=",")
np.savetxt("m1_1e-7_linear_2.csv", np.real(m1), delimiter=",")
"""
